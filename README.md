# gmihtml
[![coverage report](https://gitlab.com/jagger27/gmi-html/badges/master/coverage.svg)](https://gitlab.com/jagger27/gmi-html/-/commits/master)

A Gemini-to-HTML converter written in Go. Project is under active development and the API should not be considered stable for now. Feature requests and PRs are warmly welcomed! 

# Roadmap
In no particular order or priority 
- [x] Parse tree
- [x] HTML generation
- [ ] Command line tool
- [ ] Better templates
- [ ] Better error handling
- [ ] Better tests
- [ ] Documentation
- [ ] Examples
- [ ] Logo
- [ ] Go module

# Usage
Install
```
$ go get gitlab.com/jagger27/gmihtml
```

Simple example with defaults:
```go
import (
    "fmt"

    "gitlab.com/jagger27/gmihtml"
)

func main() {
    f, _ := os.Open("somefile.gmi")

    // Pass nil to the second argument to use default templates.
    gmi := gmihtml.NewFromReader(f, nil)
    
    result := gmi.Generate()
    fmt.Println(result)
}
```

Custom templates:

See `scanner.tmpl` for an example.
