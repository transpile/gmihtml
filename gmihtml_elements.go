package gmihtml

import (
	"bufio"
	"bytes"
	"errors"
	"fmt"
	"html/template"
	"io"
	"path"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
)

// An Element is a single element of a GMI file. It is roughly equivalent to an
// HTML element.
type Element interface {
	Type() ElementType
	Raw() string
}

// ElementType indicates the type of an Element.
type ElementType string

// Element types
const (
	Text         ElementType = "text"
	Link         ElementType = "link"
	Preformatted ElementType = "preformatted"
	Heading      ElementType = "heading"
	ListItem     ElementType = "listitem"
	Quote        ElementType = "quote"
)

var (
	linkRegexp              *regexp.Regexp = regexp.MustCompile(`^=>\s*(\S+)\s*(.*)$`)
	headingRegexp                          = regexp.MustCompile(`^(#+)\s*(.+)$`)
	quoteRegexp                            = regexp.MustCompile(`^>\s*(.+)$`)
	listItemRegexp                         = regexp.MustCompile(`^\*\s*(.+)$`)
	preformattedRegexp                     = regexp.MustCompile(`(?m)^\x60{3}[\t ]*(.*)$|(?m)^(.*)$`)
	preformattedStartRegexp                = regexp.MustCompile(`(?m)^\x60{3}[\t ]*(.*)$`)
)

var regexps = map[ElementType]*regexp.Regexp{
	Link:         linkRegexp,
	Heading:      headingRegexp,
	Quote:        quoteRegexp,
	ListItem:     listItemRegexp,
	Preformatted: preformattedStartRegexp,
}

type Container struct {
	Element
	Children []*Container
	Parent   *Container
	Template *template.Template
	Name     string
}

type GMI struct {
	Tree     *Container
	Template *template.Template
}

func (g *GMI) Generate() string {
	builder := &strings.Builder{}
	g.Template.ExecuteTemplate(builder, "index", g)
	return builder.String()
}

func splitPreformatted(data []byte, atEOF bool) (advance int, token []byte, err error) {
	advance, token, err = bufio.ScanLines(data, atEOF)
	if err == nil && token != nil {
		if preformattedStartRegexp.Match(token) {
			// idea taken from bufio.ScanLines
			if jump := bytes.Index(data[advance:], []byte("```")); jump >= 0 {
				if nextcr := bytes.IndexByte(data[advance+jump:], '\n'); nextcr >= 0 {
					jump = advance + jump + nextcr
					return jump + 1, data[0:jump], nil
				}
				// if the file ends with ``` and no cr
				return advance + jump + 3, data[0 : advance+jump+3], nil
			}
			// if there is no matching ```
			return len(data), data, nil
		}
	}
	return
}

func NewFromReader(gmi io.Reader, tmpl *template.Template) *GMI {
	if tmpl == nil {
		p, _ := filepath.Abs("../gmihtml/")
		tmpl = template.Must(template.ParseFiles(path.Join(p, "scanner.tmpl"), path.Join(p, "index.tmpl")))
	}
	s := bufio.NewScanner(gmi)
	s.Split(splitPreformatted)

	root := &Container{Template: tmpl, Name: "root"}
	current := root
	var el Element
	keep := false
	running := true
	for s.Err() == nil && running {
		if !keep {
			running = s.Scan()
			el = NewElement(s.Text())
		}
		switch current.Name {
		case "div":
			switch el.Type() {
			case Heading:
				// check depth against current node
				if el.(HeadingElement).depth <= current.Element.(HeadingElement).depth {
					// keep until proper depth
					current = current.Parent
					keep = true
					continue
				}
				current = current.addContainerChild("div")
				current.Element = el
			case ListItem:
				current = current.addContainerChild("ul")
				current.addElementChild(el)
			default:
				current.addElementChild(el)
			}
		case "ul":
			switch el.Type() {
			case ListItem:
				current.addElementChild(el)
			default:
				current = current.Parent
				keep = true
				continue
			}
		case "root":
			switch el.Type() {
			case Heading:
				current = current.addContainerChild("div")
				current.Element = el
			case ListItem:
				current = current.addContainerChild("ul")
				current.addElementChild(el)
			default:
				current.addElementChild(el)
			}
		}
		keep = false
	}

	return &GMI{Tree: root, Template: tmpl}
}

// Creates a new Container, adds it to its Children, and returns a pointer
// to it.
func (c *Container) addElementChild(el Element) *Container {
	child := &Container{
		Element:  el,
		Parent:   c,
		Template: c.Template,
		Name:     string(el.Type()),
	}
	c.Children = append(c.Children, child)
	return child
}

func (c *Container) addContainerChild(template string) *Container {
	child := &Container{
		Element:  nil,
		Parent:   c,
		Template: c.Template,
		Name:     template,
	}
	c.Children = append(c.Children, child)
	return child
}

func (c *Container) Print() {
	c.printTab(0)
}

func (c *Container) printTab(tab int) {
	indent := ""
	for i := 0; i < tab; i++ {
		indent += "  "
	}
	fmt.Print(indent)
	if c.Element == nil {
		fmt.Println(c.Name)
	} else {
		html, _ := ToHTML(c.Template, c.Element)
		fmt.Printf("%s: %s\n", html, c.Name)
	}

	for _, child := range c.Children {
		child.printTab(tab + 1)
	}
}

func (c *Container) Generate() template.HTML {
	builder := &strings.Builder{}
	c.Template.ExecuteTemplate(builder, c.Name, c)
	return template.HTML(builder.String())
}

// NewElement determines the element type from a raw string and creates
// the appropriate Element, or returns an error.
func NewElement(raw string) Element {
	matchType := Text
	for etype, regex := range regexps {
		if regex.MatchString(raw) {
			matchType = etype
			break
		}
	}
	var el Element
	switch matchType {
	case Link:
		el = newLinkElement(raw)
	case Heading:
		el = newHeadingElement(raw)
	case Quote:
		el = newQuoteElement(raw)
	case ListItem:
		el = newListItemElement(raw)
	case Preformatted:
		el = newPreformattedElement(raw)
	default:
		el = newTextElement(raw)
	}
	return el
}

// TextElement is the most basic element in a Gemini document. If it isn't
// another element, it's a TextElement.
type TextElement struct {
	raw string
}

func newTextElement(raw string) Element {
	return TextElement{raw: raw}
}

// Type implements part of the Element interface
func (el TextElement) Type() ElementType {
	return Text
}

// Raw implements part of the Element interface
func (el TextElement) Raw() string {
	return el.raw
}

// LinkElement is an element that links to another resource, with an optional
// label
type LinkElement struct {
	raw    string
	target string
	label  string
}

func newLinkElement(raw string) Element {
	el := LinkElement{raw: raw}
	matches := linkRegexp.FindStringSubmatch(el.raw)
	el.target = matches[1]
	el.label = strings.TrimSpace(matches[2])
	if el.label == "" {
		el.label = el.target
	}

	return el
}

// Raw implements part of the Element interface
func (el LinkElement) Raw() string {
	return el.raw
}

// Type implements part of the Element interface
func (el LinkElement) Type() ElementType {
	return Link
}

// Target is a LinkElement's link path
func (el LinkElement) Target() template.URL {
	return template.URL(el.target)
}

// Label is a LinkElement's link label (optional)
func (el LinkElement) Label() string {
	return el.label
}

// HeadingElement represents headings and their depth
type HeadingElement struct {
	raw   string
	depth int
	text  string
}

func newHeadingElement(raw string) Element {
	el := HeadingElement{raw: raw}
	matches := headingRegexp.FindStringSubmatch(el.raw)
	el.depth = len(matches[1])
	el.text = strings.TrimSpace(matches[2])
	return el
}

// Hx returns a heading tag name between 'h1' and 'h5' to be used verbatin in a
// template. Caller is responsible for closing the opening tag.
func (el HeadingElement) Hx() []template.HTML {
	x := strconv.Itoa(el.depth)
	if el.depth <= 5 {
		return []template.HTML{
			template.HTML("<h" + x),
			template.HTML("</h" + x + ">"),
		}
	}
	return []template.HTML{
		template.HTML("<h5"),
		template.HTML("</h5>"),
	}
}

// Text is the trimmed heading content without # prefixes
func (el HeadingElement) Text() string {
	return el.text
}

// Raw implements part of the Element interface
func (el HeadingElement) Raw() string {
	return el.raw
}

// Type implements part of the Element interface
func (el HeadingElement) Type() ElementType {
	return Heading
}

// QuoteElement is a simple text element that stands out from other text elements
type QuoteElement struct {
	raw  string
	text string
}

func newQuoteElement(raw string) Element {
	el := QuoteElement{raw: raw}
	matches := quoteRegexp.FindStringSubmatch(el.raw)
	el.text = strings.TrimSpace(matches[1])

	return el
}

// Raw implements part of the Element interface
func (el QuoteElement) Raw() string {
	return el.raw
}

// Type implements part of the Element interface
func (el QuoteElement) Type() ElementType {
	return Quote
}

// Text is the trimmed heading content without > prefix
func (el QuoteElement) Text() string {
	return el.text
}

// ListItemElement is a simple text element that stands out from other text elements
type ListItemElement struct {
	raw  string
	text string
}

func newListItemElement(raw string) Element {
	el := ListItemElement{raw: raw}
	matches := listItemRegexp.FindStringSubmatch(el.raw)
	el.text = strings.TrimSpace(matches[1])

	return el
}

// Raw implements part of the Element interface
func (el ListItemElement) Raw() string {
	return el.raw
}

// Type implements part of the Element interface
func (el ListItemElement) Type() ElementType {
	return ListItem
}

// Text is the trimmed heading content without > prefix
func (el ListItemElement) Text() string {
	return el.text
}

// PreformattedElement is a special element that in .gmi files acts as a toggle
// for preformatted text. The text that follows ``` is not to be displayed.
// The body of the element must be supplied separately.
type PreformattedElement struct {
	raw  string
	alt  string
	Body template.HTML
}

func newPreformattedElement(raw string) Element {
	el := PreformattedElement{raw: raw}
	matches := preformattedRegexp.FindAllStringSubmatch(el.raw, -1)
	el.alt = matches[0][1]
	b := strings.Builder{}
	for _, m := range matches {
		if m[2] != "" {
			b.WriteString(m[2])
			b.WriteString("\n")
		}
	}
	el.Body = template.HTML(b.String())

	return el
}

// Raw implements part of the Element interface
func (el PreformattedElement) Raw() string {
	return el.raw
}

// Type implements part of the Element interface
func (el PreformattedElement) Type() ElementType {
	return Preformatted
}

// Alt is the alternate text that follows the ```
func (el PreformattedElement) Alt() template.HTMLAttr {
	return template.HTMLAttr(el.alt)
}

// ToHTML uses a template to convert an Element into an HTML-wrapped string
func ToHTML(tmpl *template.Template, el Element) (template.HTML, error) {
	t := tmpl.Lookup(string(el.Type()))
	if t == nil {
		return "", errors.New("No such template \"" + string(el.Type()) + "\"")
	}
	result := &strings.Builder{}
	err := t.Execute(result, el)
	if err != nil {
		return "", err
	}

	return template.HTML(result.String()), nil
}
